"use strict";

/*Теоретичні питання

Опишіть своїми словами, що таке метод об'єкту. - Метод об'єкта - це значення властивості об'єкта у вигляді функції.

Який тип даних може мати значення властивості об'єкта? - Значення властивості об'єкта можуть бути будь-якого типу.

Об'єкт це посилальний тип даних. Що означає це поняття? - Об’єкт зберігає не саме значення, а його "адресу в пам’яті" – іншими словами "посилання" на нього. 
Тоді як примітивні типи даних завжди зберігають саме значенням.*/

// Завдання

function createNewUser() {
  let firstName = prompt("Enter your name.");
  let lastName = prompt("Enter your surname.");
  return {
    firstName,
    lastName,
    getLogin: function () {
      return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    },
  };
}

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
